<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>CRUD with PHP OOP & PDO</title>
	<link rel="stylesheet" href="inc/bootstrap.min.css">
	<link rel="stylesheet" href="style.css">
	<script src="inc/jquery.min.js"></script>
	<script src="inc/bootstrap.min.js"></script>
</head>
<body>
	<div class="container">
		<div class="well text-center">
			<h1>Dynamic CRUD with PHP OOP & PDO</h1>
		</div>
